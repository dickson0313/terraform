provider "aws" {
	access_key = "${var.aws_access_key}"
	secret_key = "${var.aws_secret_key}"
	region = "${var.aws_default_region}"
}

resource "aws_vpc" "terraform-vpc" {
	cidr_block = "192.168.110.0/24"
	tags {
		Name = "terraform-vpc"
	}
}

resource "aws_internet_gateway" "terraform-internet-gw" {
	vpc_id = "${aws_vpc.terraform-vpc.id}"
	tags {
		Name = "terraform-internet-gw"
	}
}

resource "aws_route_table" "terraform-route-table" {
	vpc_id = "${aws_vpc.terraform-vpc.id}"
	tags {
		Name = "terraform-route-table"
	}
}

resource "aws_route" "terraaform-internet-access" {
	route_table_id = "${aws_vpc.terraform-vpc.main_route_table_id}"
	destination_cidr_block = "0.0.0.0/0"
	gateway_id = "${aws_internet_gateway.terraform-internet-gw.id}"

}

resource "aws_subnet" "terraform-subnet" {
	vpc_id = "${aws_vpc.terraform-vpc.id}"
	cidr_block = "192.168.110.0/24"
	tags {
		Name = "terraform-subnet"
	}
}

resource "aws_security_group" "terraform-testvm-sg" {
	name = "terraform-testvm-sg"
	vpc_id  = "${aws_vpc.terraform-vpc.id}"
	#SSH access from anywhere
	ingress {
		from_port = 22
		to_port = 22
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	#HTTP access from anywhere
	ingress {
		from_port = 80
		to_port = 80
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	#HTTPS access from anywhere
	ingress {
		from_port = 443
		to_port = 443
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	#Outbound Any
	egress {
		from_port = 0
		to_port = 0
		protocol = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
	tags {
		Name = "terraform-testvm-sg"
	}
}


resource "aws_instance" "terraform-testvm"{
  ami = "${var.aws_default_ami}"
  instance_type = "${var.aws_instance_type}"
  vpc_security_group_ids = ["${aws_security_group.terraform-testvm-sg.id}"]
  key_name = "${var.aws_key_pair}"
  subnet_id = "${aws_subnet.terraform-subnet.id}"
  tags {
      Name = "TerraformTestVM"
  }
}

