output "terraform-testvm-dns" {
	value = "${aws_instance.terraform-testvm.public_dns}"
}

output "terraform-testvm-publicip" {
	value = "${aws_instance.terraform-testvm.public_ip}"
}