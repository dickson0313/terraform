variable "aws_default_region" {
	type = "string"
	default = "ap-southeast-1"
}

variable "aws_access_key" {
	type = "string"
}

variable "aws_secret_key" {
	type = "string"

}

variable "aws_default_ami" {
	type = "string"
	default = "ami-05868579"
}

variable "aws_instance_type" {
	type = "string"
	default = "t2.micro"
}

variable "aws_key_pair" {
	type = "string"
	default = "Dickson-key"
}

