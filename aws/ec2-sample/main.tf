provider "aws" {
  access_key = ""
  secret_key = ""
  region = "ap-southeast-1"
}

resource "aws_instance" "testvm"{
  ami = "ami-05868579"
  instance_type = "t2.micro"
  tags {
      Name = "TerraformTestVM"
  }
}
