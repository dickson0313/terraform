provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "${var.aws_default_region}"
}

#Create VPC1
resource "aws_vpc" "Terraform_VPC1" {
	cidr_block = "10.40.180.0/24"
	
	tags{
		Name = "Terraform_VPC1"
	}
}
#Create VPC2
resource "aws_vpc" "Terraform_VPC2" {
	cidr_block = "10.40.181.0/24"
	tags {
		Name = "Terraform_VPC2"
	}
}
#Form Peering VPC1 -> VPC2
resource "aws_vpc_peering_connection" "Terraform_VPC1-to-Terraform_VPC2" {
	peer_vpc_id = "${aws_vpc.Terraform_VPC2.id}"
	vpc_id = "${aws_vpc.Terraform_VPC1.id}"
	tags {
		Name = "Terraform_VPC1-to-Terraform_VPC2"
	}
}
#Form Peering VPC2 -> VPC1
resource "aws_vpc_peering_connection" "Terraform_VPC2-to-Terraform_VPC1" {
	peer_vpc_id = "${aws_vpc.Terraform_VPC1.id}"
	vpc_id = "${aws_vpc.Terraform_VPC2.id}"
	tags {
		Name = "Terraform_VPC2-to-Terraform_VPC1"
	}
}



