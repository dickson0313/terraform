variable "azure-client-id" {
	type = "string"
}

variable "azure-client-secret" {
	type = "string"
}

variable "azure-subscription-id" {
	type = "string"
	default = "07ea19a1-4fca-4f91-8e0e-ae674d212e86"
}

variable "azure-tenant-id" {
	type = "string"
	default = "15c12a83-aebd-4087-9575-0d5d30a6208b"
}
