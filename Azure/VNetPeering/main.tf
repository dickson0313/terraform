provider "azurerm" {
	subscription_id = "${var.azure-subscription-id}"
	client_id = "${var.azure-client-id}"
	client_secret = "${var.azure-client-secret}"
	tenant_id = "${var.azure-tenant-id}"
}

resource "azurerm_resource_group" "terraform-test-rg" {
	name = "terraform-test-rg"
	location = "East Asia"
}

resource "azurerm_virtual_network" "terraform-vnet-HUB" {
	name = "terraform-vnet-HUB"
	address_space = ["10.46.10.0/24"]
	location = "${azurerm_resource_group.terraform-test-rg.location}"
	resource_group_name = "${azurerm_resource_group.terraform-test-rg.name}"
	
	subnet {
		name = "default"
		address_prefix = "10.46.10.0/24"
	}
}

resource "azurerm_virtual_network" "terraform-vnet-Spoke" {
	name = "terraform-vnet-Spoke"
	address_space = ["10.46.11.0/24"]
	location = "${azurerm_resource_group.terraform-test-rg.location}"
	resource_group_name = "${azurerm_resource_group.terraform-test-rg.name}"
	
	subnet {
		name = "default"
		address_prefix = "10.46.11.0/24"
	}
}

resource "azurerm_virtual_network_peering" "hub-to-spoke" {
	name = "terraform-hub-to-spoke"
	resource_group_name = "${azurerm_resource_group.terraform-test-rg.name}"
	virtual_network_name = "${azurerm_virtual_network.terraform-vnet-HUB.name}"
	remote_virtual_network_id = "${azurerm_virtual_network.terraform-vnet-Spoke.id}"
}

resource "azurerm_virtual_network_peering" "spoke-to-hub" {
	name = "terraform-spoke-to-hub"
	resource_group_name = "${azurerm_resource_group.terraform-test-rg.name}"
	virtual_network_name = "${azurerm_virtual_network.terraform-vnet-Spoke.name}"
	remote_virtual_network_id = "${azurerm_virtual_network.terraform-vnet-HUB.id}"
}
